#!/bin/bash
# Main runner for the pop-pop program. v.0.5.
# Runs admixture for a number of K values, then starts the rshiny app for user.
# Usage: pop-main.sh --
# Exmple usage:
# ./pop-main.sh -f Data/takeuchi2020/takeuchi.bed -l Data/takeuchi2020/site_coordinates.tsv -m Data/takeuchi2020/migration_matrix.txt -n takeuchi
# Relies on: admixture, rshiny (see rshiny file for R libraries used)

############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo This is the main runner for the program. It launches admixture and then the rshiny app which gives you nice visualisation.
   echo Version 0.5
   echo ~~~~~~~~~
   echo "-f: path to variant file, must be plink .bed file or plink '12' coded .ped file"
   echo     "(with .fam and .bim files also existing in the same directory, see ADMIXTURE manual)"
   echo     "Furthermore, admixure does not accept non-integer chromosome names. Fix this beforehand in your plink files."
   echo "-l: path to sample location file, tab separated with one column called Latitude, one called Longitude, and one called Locality_ID"
   echo "-m: path to Relative directional Migration matrix (from divMigrate). Optional."
   echo "-n: name of this run. Output directories and files will have this as skeleton name. Keep it simple."
   echo "-h: print help and exit."
}

# Note that ADMIXTURE infers the format of your input file based on the file extension (.bed, .ped, or .geno).

############################################################
# Process the input options.                               #
############################################################
# Flag indicating if matrix was submitted:
muse=0
# Get the options
while getopts ":f:l:m:n:h" OPTION
do
    case "$OPTION" in
        f)
            echo Path to variant .bed file set to $OPTARG
            varfile=$OPTARG
            ;;
        l)
            echo Path to location file set to $OPTARG
            locfile=$OPTARG
            ;;
        m)
            echo Path to Relative directional Migration matrix is $OPTARG
            matr=$OPTARG
            muse=1
            ;;
        n)
            echo Run name is $OPTARG
            runn=$OPTARG
            ;;
        h)
            Help
            exit
            ;;
        ?)
            echo $OPTARG is not a valid option.
            exit
            ;;
    esac
done

############################################################
# Main program                                             #
############################################################

#### Get file directory
plinkdir=${varfile%/*}
infileskeleton=${varfile##*/}
infileskeleton=${infileskeleton%.bed}
# Create name of output directory:
outdir=run_${runn}

#### Get values for K ####
numpops=$(cat ${locfile} | wc -l)
mink=2
maxk=$(echo ${numpops}+1 | bc) # For testing purposes, comment this line and uncomment the next line
#maxk=3

echo "Number of sampled populations:" ${numpops}
echo "Will try values for K:" ${mink}-${maxk}
echo 

#### run admixture ####
for ((k=${mink};k<=${maxk};k++)); do
    outfile=$(echo ${outdir}/${runn}_${k}.out);
    echo Looking for file: ${outfile};
    if [ -f ${outfile} ]; then
        echo ${outfile} exists. Will skip this K.
        echo
        continue
    fi;
    echo --------------------------------------------------------
    echo A file ${outfile} was not found. Therefore, now running admixture for K = ${k}...
    echo 'Input file: ' ${varfile};
    echo;
    admixture --cv ${varfile} ${k} -j4 > ${runn}_${k}.out;
    echo Writing sample IDs to Q file...;
    cat ${varfile%.bed}.fam | cut -f 1,2 -d ' ' | tr ' ' '\t' > temp1;
    cat ${infileskeleton}.${k}.Q | tr " " "\t" > temp2;
    paste temp1 temp2 >> ${k}.Q;
    rm temp1;
    rm temp2;
    done
echo 'Finished running admixture for all K that did not already exist.'
echo 'Since this script is being run in the program base directory, and admixture outputs are created in the'
echo 'directory where the command is being called, we now move the output files to a subdirectory...'
echo

# Check if outdir already exists and if not make it:
if [ ! -d ${outdir} ]; then
    mkdir ${outdir}
fi;

# Check if there are any files ending with .Q and if so move them to outdir:
if [ $(ls | grep .Q$ | wc -l) -gt 0 ]; then
    mv *.Q ${outdir}
fi;

# Check if there are any files ending with .P and if so move them to outdir:
if [ $(ls | grep .P$ | wc -l) -gt 0  ]; then
    mv *.P ${outdir}
fi;

# Check if there are any files ending with .out and if so move them to outdir:
if [ $(ls | grep .out$ | wc -l) -gt 0  ]; then
    mv *.out ${outdir}
fi;

echo The Q, P and log files can be found in the directory ${outdir}.
echo Do not move or rename them or that directory.
echo


#### Prepare files needed in rshiny app ####
## Convert divMigrate matrix to better matrix:
echo Looking for migration matrix
if [ ${muse} == 1 ] && [ -f ${matr} ]; then
    grep -A 10000 Pops ${matr} | tr -d "," | tr -d " " > ${outdir}/migmat.txt
    echo Found migration matrix file where you provided it.
    echo It has been processed and moved to the outdir.
    echo
else
    echo Note: You did not supply a migration matrix file, or the file you supplied was not found.
    echo No processed migration matrix will be found in the outdir.
    echo
fi

####### CHANGE WORKING DIRECTORY to go to output dir: ############
cd ${outdir}
echo Changed working directory to ${outdir}...
## Get list of K values represented:
ls *.Q | grep -o [0-9] | sort -n | uniq > Klist.txt
echo List of K-values obtained.
## Get list of CV values:
echo -e k:cv > CV_values.csv
grep 'CV' *.out | cut -f 3,4 -d ' ' | sed 's/[\w\t(\)K=]//g' >> CV_values.csv
echo Table of CV-values obtained.
## Get path to location file:
echo ../${locfile} > locpath.txt
echo Coordinates file path obtained.

echo
echo All files for R have been prepared. In a moment, R will launch.
echo Please ensure you have a working R installation on your system,
echo and that all R packages listed in the pop-pop readme are installed.
echo "When the R-Shiny app is loaded, please click on the URL where it says 'Listening on' below:"
echo

# Change wd to rshiny directory:
cd ../pop-rshiny

#### Launch the shiny app in the current directory (obs directory can be changed):
R -e "shiny::runApp('./')"
