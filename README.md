# pop-pop: The population structure visualiser and explorer
This is a program that solves the challenge of neatly plotting data commonly presented in studies of population genetics. We commonly encounter the following types of data in our own studies and when reading other researchers' papers:
- Geolocation data (coordinates)
- Genetic variation or ancestry inferrence based on this variation
- Migration routes calculated from data on genetic variation

It would be nice to present admixture plots and migration routes on one and the same map. pop-pop does this. It can be useful to a researcher in both data exploration phase, and when prparing figures for publication.
# How it works
Pop-pop consists of two main parts; a bash script and an R-Shiny app.
## The bash script
The bash script evaluates the set of K to be analysed for a genetic variant dataset, and then calls ADMIXTURE (I used Version 1.3.0 during development and testing). ADMIXTURE estimates coefficients of ancestry for these K for each individual, see Alexander et al. 2009 for details. The bash script then conducts structuring and preparation of ADMIXTURE output and other relevant files and lastly the R-Shiny app is opened for data visualisation as described in Features section below. All output from the bash script (a ‘run’) is placed in an output directory with a user-specified name.
## The R-Shiny app
In the R-Shiny app, ggmap is used to draw the map and ggplot2 used to draw admixture plots. An algorithm was developed to (i) place admixture plots to left and right sides of the map depending on the longitude of the corresponding sample point, and (ii) utilise the full height of the map to fit admixture plots. For a full list of R packages used, see header in the R-Shiny script 'app.R'.
# Dependencies
You must have ADMIXTURE installed on your system and in your path. Check out: https://dalexander.github.io/admixture/download.html. Alternatively, it can be easily installed with conda, check out: https://anaconda.org/bioconda/admixture. Do not forget to activate the conda environment you installed it in.
## R packages used
You must also have a working installation of R: https://www.r-project.org/packages, with the following packages installed:
- shiny
- ggmap
- ggplot2
- grid
- gridExtra
- reshape2
- stringr
# How to run
## Step 1: run pop-main.sh
You would usually start with the bash script. The bash script launches the R-Shiny app. Example call:
```bash
./pop-main.sh -f Data/takeuchi2020/takeuchi.bed -l Data/takeuchi2020/site_coordinates.tsv -m Data/takeuchi2020/migration_matrix.txt -n takeuchi
```
Where
- `-f` takes the path (relative to current working directory) to a genetic variant file in PLINK format. Path must lead to a .bed file, and the same directory must contain the corresponding .bim file and the corresponding .fam file. See the PLINK help pages: https://www.cog-genomics.org/plink/1.9/formats for details on PLINK format, and see section Tips and Tricks below for how to convert between common formats. The reason for these specifications is simply that this is the format that ADMIXTURE requires. See the ADMIXTURE manual: https://dalexander.github.io/admixture/admixture-manual.pdf for details. This is not optional.
- `-l` takes the path (relative to current working directory) to a tab-separated file containing coordinates of each sample location. This file must have one column named 'Latitude' containing latitude coordinate, one column named 'Longitude' containing longitude coordinate, and one column named 'Locality_ID' containing the name of each location. This name must be the same as the one used for "family" name in the PLINK .fam file. This is not optional.
- `-m` takes the path (relative to current working directory) to a file containing a migration matrix. This file is allowed to contain metadata, but it must contain an n x n tab-separated matrix beginning with the string `Pops` where n = number of sampling sites. Significant values are marked with an asterisk * immediately after the value, and these significant values are the only ones that will be visualised later. For reference, the file should look something like the output from divMigrate online (https://popgen.shinyapps.io/divMigrate-online/). This is optional.
- `-n` takes your choice of name for this 'run'. An output directory named run_\<runname\> will be created where all output from the shell script and its ADMIXTURE call will be placed. This is not optional. 

The directory 'Data' in the repository contains some sample data sets. They all have PLINK files and site_coordinates.tsv files which can be studied to make sure your data is in the same format. The example data gony0000 also has a matrix.txt with a sample divMigrate online output.

Running ADMIXTURE takes a long time. It is actually advised you run pop-pop on a cluster instead. Or take a lunch break. Alternatively, you may be here just for the visualisation, or because you want to grade the author. If so, you can start the R-Shiny app straight away by heading to step 2.

## Step 2: open R-Shiny server in browser
When ADMIXTURE has run for all relevant K, the shell script launches R in the same terminal window. R will start a local R-Shiny server which is accessible in your web browser by going to the URL specified where it says 'Listening on' in the terminal output. After you finish using the app, close your browser and in the terminal simply cancel the process (usually ctrl+c).
### Manual R-Shiny launch.
If you just want to launch the R-Shiny app without running the shell script (for example if you already have conducted the run), then use the same command that the shell script uses:
```bash
R -e "shiny::runApp('./')"
```
(While in the pop-rshiny directory!)

Then go to the URL presented by R in the terminal output.

## Step 3: use interactively
Once you have a browser window open with the pop-pop interface, you are greeted with three main sections:
- To the left there is a settings panel, featuring:
  1. A drop-down menu where the user chooses the 'run' to explore. A run directory consists of the complete output from a run of the pop-main.sh bash script, and must be named run_\<runname\>. Some example run directories are supplied.
  2. A drop-down menu where the user chooses the K for which to draw admixture plots on the map.
  3. A sliding bar where the user determines the threshold value for migration arrows. Migration routes with coefficients below the threshold are not drawn.
- To the right there are two tabs:
  - The first tab ('Run info') will provide the user with a plot of the cross-validation (CV) values provided from ADMIXTURE. This will inform the user's choice of K for which to draw admixture plots. Also the table of coordinates supplied by the user is drawn in this tab. The reason for this is because some users may have additional information in this table, such as sample size or phenotype, which may be useful to keep for quick reference.
  - The second tab ('Map') will provide the main feature: a map is drawn with admixture plots along each side, connected to sample locations by indicator lines. If a migration matrix was supplied, the map also displays arrows between sample locations, visualising migration routes (pointing direction) and magnitude of the migration coefficient (arrow width).
# Example data
The directory Data contains some example data directories. The directories run_* supplied in the repository each contain the results of a run of pop-pop corresponding to the example data. Two example data sets were downloaded from the dryad entries of the following studies:
- Takeuchi T et al. Divergent northern and southern populations and demographic history of the pearl oyster in the western Pacific revealed with genomic SNPs. Evol Appl 2020;13:837–53.
- Van Wyngaarden M et al. Identifying patterns of dispersal, connectivity and selection in the sea scallop, Placopecten magellanicus, using RADseq-derived SNPs. Evol Appl 2017;10:102–17.

The other two example data sets have other sources:
- "maritime" is course data from canvas.
- "gony" is data provided by Karin Rengefors and Dag Ahrén.

OBS! These last two do not exist in the GitLab repository yet because I was unsure if they should be public.

# Known drawbacks/bugs
There are some known drawbacks that the user should be aware of:
- It is not a forgiving program when it comes to file formats. User must follow instructions above carefully to ensure their input files are correct format.
- Sample names in coordinates table (as well as in .fam file since these should be identical) benefit from being short. This is because of a bug in ggplot aesthetics inheritance prohibiting conditional axis title size and angle.
- The nature of geographical coordinates causes some features of the map to be imperfect. The positions of the admixture plots are calculated based on their vertical order and the total height of the map. Therefore, height of these plots increases the further north the corresponding point is. This effect is more prominent the bigger area and/or the further north the map covers.
- Migration arrows would benefit from being curved, not straight, so they can be discerned easier. This is easier said than done with the currently used graphics package (ggplot2 + ggmap) because of the discrepancy between cartesian and geographical coordinates.
- Both of these last two points could be remedied by converting plots to cartesian, but this a) potentially distorts the map image and b) without proper conversion of the coordinates of the sample points themselves, they end up in the wrong location.
# Roadmap
Some features that I hope to add in a future release are:
- [ ] Ability to input other variant data formats, for example .vcf, and use PLINK to convert.
- [ ] Automatic estimation of migration coefficient and direction. For example, implementation of divMigrate within pop-pop. There is already an R package supplying divMigrate functionality (https://www.rdocumentation.org/packages/diveRsity/versions/1.9.89) which could be added to the R-Shiny script. It is outdated at the time of writing which is why it was not implemented in the current version of pop-pop.
- [ ] Zoomable+pannable map and map elements displaying tooltips upon cursor hoover. This would require heavy rewriting of the map section of the R-Shiny app.
- [ ] Automatic environment creation and activation for ADMIXTURE installation and execution (and subsequent environment deactivation).
- [ ] Automatic installation of R packages
- [ ] Toggleable map features: turn migration arrows, admix plots, location labels on/off at the click of a radio button.
- [ ] Use absolute paths in bash script by making a variable=pwd in the beginning of the script. Adjust throughout script to use this variable instead.
- [DONE] Fix bash scripts so it checks if run or K for that run already has been conducted
# Tips and tricks
## How to convert .ped to .bed
Let us say you have the two files:
```
takeuchi.ped
takeuchi.map
```
But you want the files
```
takeuchi2.bed
takeuchi2.bim
takeuchi2.fam
```
for example for running ADMIXTURE as implemented in pop-pop.

Use plink to convert:
```
plink --file takeuchi --out takeuchi2 --make-bed --allow-extra-chr
```
## How to convert vcf to plink:
Let us say you have the file:
```
takeuchi.vcf
```
But you want the files
```
takeuchi2.bed
takeuchi2.bim
takeuchi2.fam
```
for example for running ADMIXTURE as implemented in pop-pop.
Use plink to convert:
```bash
plink --vcf takeuchi.vcf --make-bed --out takeuchi2 --allow-extra-chr
```
## How to remove invalid chromosome names before running ADMIXTURE:
ADMIXTURE only takes files with human chromosome names. For non-human organisms, this can lead to error. The following line was adapted from the ADMIXTURE tutorial at https://speciationgenomics.github.io/ADMIXTURE/. It replaces all chromosome names in your .bim file with '0'.
```bash
awk '{$1="0";print $0}' takeuchi2.bim > takeuchi2.bim.tmp
mv takeuchi2.bim.tmp takeuchi2.bim
```